<?php
/**
 * @file
 * menu_access_key.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function menu_access_key_eck_bundle_info() {
  $items = array(
    'menu_access_key_menu_access_key' => array(
      'machine_name' => 'menu_access_key_menu_access_key',
      'entity_type' => 'menu_access_key',
      'name' => 'menu_access_key',
      'label' => 'Menu access key',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function menu_access_key_eck_entity_type_info() {
  $items = array(
    'menu_access_key' => array(
      'name' => 'menu_access_key',
      'label' => 'Menu access key',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'language' => array(
          'label' => 'Entity language',
          'type' => 'language',
          'behavior' => 'language',
        ),
      ),
    ),
  );
  return $items;
}
