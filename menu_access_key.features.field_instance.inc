<?php
/**
 * @file
 * menu_access_key.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function menu_access_key_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'menu_access_key-menu_access_key-field_menu_access_key_key'
  $field_instances['menu_access_key-menu_access_key-field_menu_access_key_key'] = array(
    'bundle' => 'menu_access_key',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A random key that will be used to bypass menu link access. One has automatically being suggested for you.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'menu_access_key',
    'field_name' => 'field_menu_access_key_key',
    'label' => 'Key',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'menu_access_key-menu_access_key-field_menu_access_key_menu'
  $field_instances['menu_access_key-menu_access_key-field_menu_access_key_menu'] = array(
    'bundle' => 'menu_access_key',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'menu_access_key',
    'field_name' => 'field_menu_access_key_menu',
    'label' => 'Menu item',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A random key that will be used to bypass menu link access. One has automatically being suggested for you.');
  t('Key');
  t('Menu item');

  return $field_instances;
}
